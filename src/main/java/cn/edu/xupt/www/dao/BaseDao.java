package cn.edu.xupt.www.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BaseDao {
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }
    public Connection getCon() throws SQLException{
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/users","root","root");
        return con;
    }
    public void closeAll(Connection con, PreparedStatement ps)throws Exception{
        if(con!=null){
            con.close();
        }
        if(ps!=null){
            ps.close();
        }
    }
}
