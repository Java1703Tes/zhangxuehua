package cn.edu.xupt.www.dao;

import cn.edu.xupt.www.utils.UserBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UserDaoimpl extends BaseDao implements IUserDao {
    public int insert(UserBean user) throws Exception {
       Connection con = null;
       PreparedStatement ps = null;
       try{
           con = this.getCon();
           ps = con.prepareStatement("insert into users (uname,pass) values(?,?)");
           ps.setString(1,user.getUname());
           ps.setString(2,user.getPass());
           int num = ps.executeUpdate();
           return num;
       }catch(Exception e){
           e.printStackTrace();
       }finally{
           this.closeAll(con ,ps);
       }
        return 0;
    }

    public int delete(int uid) throws Exception {
        return 0;
    }

    public int update(UserBean user) throws Exception {
        return 0;
    }

    public List<UserBean> selectAll() throws Exception {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con = this.getCon();
            ps = con.prepareStatement("select * from users");
            rs = ps.executeQuery();
            List<UserBean> userList = new ArrayList<UserBean>();
            UserBean user = null;
            while(rs.next()){
                user = new UserBean();
                user.setId(rs.getInt(1));
                user.setUage(rs.getInt(2));
                user.setUname(rs.getString(3));
                user.setSex(rs.getString(4));
                user.setUadd(rs.getString(5));
                user.setPass(rs.getString(6));
                userList.add(user);
            }
            return userList;
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            if(rs!=null){
                rs.close();
            }
            this.closeAll(con,ps);
        }
        return null;
    }

    public UserBean selectById(int uid) throws Exception {
        return null;
    }
}
