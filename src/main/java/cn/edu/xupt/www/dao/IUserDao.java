package cn.edu.xupt.www.dao;

import cn.edu.xupt.www.utils.UserBean;

import java.util.List;

public interface IUserDao {
   public int insert(UserBean user)throws Exception;
   public int delete (int uid) throws Exception;
   public int update (UserBean user)throws Exception;
   public List<UserBean> selectAll()throws Exception;
   public UserBean selectById(int uid)throws Exception;

}
