package cn.edu.xupt.www.utils;

import java.io.Serializable;

public class UserBean implements Serializable{
    private static final long serialVerdsionUID = 1L;
    private int id;
    private int uage;
    private String sex;
    private String uname;
    private String pass;
    private String uadd;

    public String getUadd() {
        return uadd;
    }

    public void setUadd(String uadd) {
        this.uadd = uadd;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }



    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public int getUage() {
        return uage;
    }

    public void setUage(int uage) {
        this.uage = uage;
    }

    public int getId() {
        return id;

    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "id=" + id +
                ", uage=" + uage +
                ", uname='" + uname + '\'' +
                ", pass='" + pass + '\'' +
                ", uadd='" + uadd + '\'' +
                '}';
    }
}
