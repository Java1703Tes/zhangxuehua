package cn.edu.xupt.www.controller;
import cn.edu.xupt.www.dao.UserDaoimpl;
import cn.edu.xupt.www.utils.UserBean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;


public class MyServlet_login extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uname = req.getParameter("username");
        String pass = req.getParameter("password");
        List<UserBean> arrayList = null;
        try {
            arrayList = new UserDaoimpl().selectAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Iterator it = arrayList.iterator();
        while(it.hasNext()){
            UserBean key = (UserBean) it.next();
            if((key.getUname() .equals(req.getParameter(uname)))&&(key.getPass().equals(req.getParameter(pass)))){
                req.getRequestDispatcher("home.jsp").forward(req,resp);
            }else{
                req.getRequestDispatcher("login_error.jsp").forward(req,resp);

            }
        }
    }
}
